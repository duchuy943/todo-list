import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './Login.html';

Template.login.events({
  'submit .login-form'(event) {
    event.preventDefault();
    const username = event.target.username.value;
    const password = event.target.password.value;

    Meteor.loginWithPassword(username, password);
  },
});
