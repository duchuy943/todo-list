import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Tracker } from 'meteor/tracker';
import { ReactiveDict } from 'meteor/reactive-dict';

import TasksCollection from '/imports/db/TasksCollection';

import './App.html';
import './Task.js';
import './Login.js';

const HIDE_COMPLETED_STRING = 'hideCompleted';
const IS_LOADING_STRING = 'isLoading';

const getTasksFilter = () => {
  const user = Meteor.user();
  const hideCompletedFilter = { isChecked: { $ne: true } };
  const userFilter = user ? { userId: user._id } : {};
  const pendingOnlyFilter = { ...hideCompletedFilter, ...userFilter };

  return { userFilter, pendingOnlyFilter };
};

/**
 * A ReactiveDict stores an arbitrary set of key-value pairs.
 * Use it to manage the internal state in your components.
 */
Template.mainContainer.onCreated(function mainContainerOnCreated() {
  this.state = new ReactiveDict();
  const handler = Meteor.subscribe('tasks');
  Tracker.autorun(() => {
    this.state.set(IS_LOADING_STRING, !handler.ready());
  });
});

Template.mainContainer.helpers({
  tasks() {
    const instance = Template.instance();
    const hideCompleted = instance.state.get(HIDE_COMPLETED_STRING);
    const { userFilter, pendingOnlyFilter } = getTasksFilter();

    if (!Meteor.user()) {
      return [];
    }

    return TasksCollection.find(hideCompleted ? pendingOnlyFilter : userFilter, {
      sort: { createdAt: -1 },
    }).fetch();
  },

  hideCompleted() {
    return Template.instance().state.get(HIDE_COMPLETED_STRING);
  },

  incompleteTasksCount() {
    if (!Meteor.user()) {
      return '';
    }

    const { pendingOnlyFilter } = getTasksFilter();
    const incompleteTasksCount = TasksCollection.find(pendingOnlyFilter).count();

    return incompleteTasksCount ? `(${incompleteTasksCount})` : '';
  },

  isUserLogged() {
    return !!Meteor.user();
  },

  getUser() {
    return Meteor.user();
  },

  isLoading() {
    const instance = Template.instance();
    return instance.state.get(IS_LOADING_STRING);
  },
});

Template.mainContainer.events({
  'click #hide-completed-button'(event, templateInstance) {
    const currentHideCompleted = templateInstance.state.get(HIDE_COMPLETED_STRING);
    templateInstance.state.set(HIDE_COMPLETED_STRING, !currentHideCompleted);
  },

  'click .logout'() {
    Meteor.logout();
  },
});

Template.form.events({
  'submit .task-form'(event) {
    const evt = event;
    evt.preventDefault();
    const text = evt.target.text.value;
    if (text === '') {
      return;
    }

    // Insert to Database.
    // TasksCollection.insert({
    //     text,
    //     userId: Meteor.user()._id,
    //     createdAt: new Date()
    // });
    Meteor.call('tasks.insert', text);

    // Clear form
    evt.target.text.value = '';
  },
});
