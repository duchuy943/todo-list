import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
// import TasksCollection from '/imports/db/TasksCollection';

import './Task.html';

Template.task.events({
  'click .toggle-checked'() {
    // TasksCollection.update( this._id, {
    //     $set: { isChecked: !this.isChecked },
    // })
    Meteor.call('tasks.setIsChecked', this._id, !this.isChecked);
  },
  'click .delete'() {
    // TasksCollection.remove(this._id);
    Meteor.call('tasks.remove', this._id);
  },
});

Template.task.helpers({
  checked() {
    return this.isChecked;
  },
});
